#!/bin/bash
set -e

echo ""

echo "#####################################################"
echo "Builing and deploying the resume to OCI bucket       "
echo "#####################################################"

echo "Building the project ..."
npm run build
jsscript=`find build -name "main.*.chunk.js"`

#export OCI_CLI_PROFILE=SOLARPACE

# Retrieve OCI settings from oci-exports.js
if [ -f src/oci-exports.js ]
then
  oci_namespace=`cat src/oci-exports.js|grep oci_os_namespace| cut -d: -f 2  | cut -d\" -f 2`
  oci_os_bucket=`cat src/oci-exports.js|grep oci_os_bucket| cut -d: -f 2  | cut -d\" -f 2`
else
  echo "ERROR : oci-exports.js does not exist in src folder"
  exit 1
fi


echo "Replacing static URLs in the project ..."

sed -i .bak "s/\"\/img\/resume_picture/\"\/n\/$oci_namespace\/b\/$oci_os_bucket\/o\/img\/resume_picture/g" $jsscript

sed -i .bak "s/\"\/favicon\.ico\"/\"\/n\/$oci_namespace\/b\/$oci_os_bucket\/o\/favicon\.ico\"/g" build/index.html
sed -i .bak "s/\"\/manifest\.json\"/\"\/n\/$oci_namespace\/b\/$oci_os_bucket\/o\/manifest\.json\"/g" build/index.html
sed -i .bak "s/\"\/img\.png\"/\"\/n\/$oci_namespace\/b\/$oci_os_bucket\/o\/img\.png\"/g" build/index.html
#sed -ri .bak 's/"\/img\/([a-z].*)\.png"/"\/n\/$oci_namespace\/b\/$oci_os_bucket\/o\/img\/\1\.png"/g'  static/js/main.59785d92.chunk.js.map
#sed -ri .bak 's/"\/img\/([a-z].*)\.png"/"\/n\/$oci_namespace\/b\/$oci_os_bucket\/o\/img\/\1\.png"/g' static/js/main.59785d92.chunk.js
#sed -i .bak 's/(\/img\/\([a-z].*\)\.png)/(\/n\/$oci_namespace\/b\/$oci_os_bucket\/o\/img\/\1\.png)/g' static/css/main.a75f7885.chunk.css
#sed -i .bak 's/(\/img\/\([a-z].*\)\.png)/(\/n\/$oci_namespace\/b\/$oci_os_bucket\/o\/img\/\1\.png)/g' static/css/main.a75f7885.chunk.css.map
sed -i .bak "s/\"\/static\/js\//\"\/n\/$oci_namespace\/b\/$oci_os_bucket\/o\/static\/js\//g" build/index.html
sed -i .bak "s/\"\/static\/css\//\"\/n\/$oci_namespace\/b\/$oci_os_bucket\/o\/static\/css\//g" build/index.html
sed -i .bak "s/\"static\/js\/\"/\"n\/$oci_namespace\/b\/$oci_os_bucket\/o\/static\/js\/\"/g" build/index.html
#sed -ri .bak 's/"\/img\/([a-z].*)\.png"/"\/n\/$oci_namespace\/b\/$oci_os_bucket\/o\/img\/\1\.png"/g'  static/js/main.59785d92.chunk.js.map
#sed -ri .bak 's/"\/img\/([a-z].*)\.png"/"\/n\/$oci_namespace\/b\/$oci_os_bucket\/o\/img\/\1\.png"/g' static/js/main.59785d92.chunk.js
#sed -i .bak 's/(\/img\/\([a-z].*\)\.png)/(\/n\/$oci_namespace\/b\/$oci_os_bucket\/o\/img\/\1\.png)/g' static/css/main.a75f7885.chunk.css
#sed -i .bak 's/(\/img\/\([a-z].*\)\.png)/(\/n\/$oci_namespace\/b\/$oci_os_bucket\/o\/img\/\1\.png)/g' static/css/main.a75f7885.chunk.css.map

cd build

echo "Deploying the project in Bucket $oci_os_bucket ..."

echo "--->Deleting existing files in $oci_os_bucket"
echo oci os object bulk-delete -ns $oci_namespace  -bn $oci_os_bucket  --prefix static --force
oci os object bulk-delete -ns $oci_namespace  -bn $oci_os_bucket  --prefix static --force

echo "--->Uploading new files in $oci_os_bucket"
oci os object put -bn $oci_os_bucket --file ./manifest.json --content-type application/json --force
oci os object bulk-upload -bn $oci_os_bucket --src-dir ./ --content-type text/html --include "*.html" --overwrite
oci os object bulk-upload -bn $oci_os_bucket --src-dir ./ --content-type image/jpeg --include "*.png" --overwrite
oci os object bulk-upload -bn $oci_os_bucket --src-dir ./ --content-type text/javascript --include "*.js" --overwrite
oci os object bulk-upload -bn $oci_os_bucket --src-dir ./ --content-type application/pdf --include '*.pdf"' --overwrite
oci os object bulk-upload -bn $oci_os_bucket --src-dir ./ --content-type text/css --include "*.css" --overwrite
oci os object bulk-upload -bn $oci_os_bucket --src-dir ./ --content-type text/plain --exclude "*.js" --exclude "*.html" --exclude "*.png" --exclude '*.pdf' --exclude "*.css" --exclude ./manifest.json --overwrite
