import axios from 'axios';
import configData from "./oci-exports";

const client = axios.create({
    baseURL: configData.oci_apigateway_url
  });
  

export async function  getSoda(collection) {
    const config = {headers: {
      "opc-request-id": "12345",
    }}
    const data = {collection: collection}

    const response = await client.post('', data, config);
    // Format Response to include ID
    const formattedItems = response.data.map((item) => {
      const formattedItem = {
        ...item.value,
        id: item.id
      }
      return formattedItem
    })
    return formattedItems;
  }

