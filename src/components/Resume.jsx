import React from "react";
import Career from "./sections/Career/Career";
import Profile from "./sections/Profile/Profile";
import Skill from "./sections/Skill/Skill";
import Education from "./sections/Education/Education";
import Certification from "./sections/Certification/Certification";
import Language from "./sections/Language/Language";
import Header from "./sections/Header/Header";
import Footer from "./sections/Footer/Footer";

const Resume = () => {
   return (
    <div className="container">
        <Header />
        <Profile />
        <Skill />
        <Career />
        <Education />
        <Certification />
        <Language />
        <Footer />
     </div>
   );
}

export default Resume;
