import React from "react";
import Card from "../../ui/Card";

const SkillItem = (props) => {
  let item = props.item;
  return (
    <div>
      <Card className="profiles-item">
        <div key={item.id}>
          <h4>
            <b>{item.position} </b>
          </h4>
          <ul>
            {item.accomplishments.map((itemm, i) => (
              <li key={i}>{itemm}</li>
            ))}
          </ul>
        </div>
      </Card>
    </div>
  );
};

export default SkillItem;
