import SkillItem from "./SkillItem";
import ScrollableAnchor from "react-scrollable-anchor";
import React, { useState, useEffect } from "react";
import Card from "../../ui/Card";

import { getSoda } from "../../../oci_helper";

const Skill = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    const getData = async () => {
      try {
        const profile = await getSoda("skills");
        setData(profile);
      } catch (err) {
        console.log("ERROR");
        setError(err);
        console.log(error);
      }
    };
    getData();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <Card className="profiles">
        <div className="row section">
          <ScrollableAnchor id={"sectSkill"}>
            <div className="col-xs-2">
              <h2>Skills</h2>
            </div>
          </ScrollableAnchor>
          <div className="col-xs-10">
            {data.length === 0 ? (
              <div id="spinnerHeader" className="container">
                <div className="loading"></div>
              </div>
            ) : (
              ""
            )}

            <div className="jd">
              <div className="row">
                {data.map((item) => (
                  <SkillItem key={item.id} item={item} />
                ))}
              </div>
            </div>
          </div>
        </div>
      </Card>
    </div>
  );
};

export default Skill;
