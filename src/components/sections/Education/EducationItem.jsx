import React, { Component } from "react";
import Card from "../../ui/Card";

class EducationItem extends Component {
  render() {
    let item = this.props.item;
    return (
      <div>
        <Card className="profiles">
          <div className="jd">
            <div className="row">
              <div className="col-xs-2">
                <h5>{item.dates}</h5>
              </div>
              <div className="col-xs-10">
                <h5>
                  <b>{item.title}</b>
                  {item.school.length > 2 && <span> - {item.school}</span>}
                </h5>
              </div>
            </div>
          </div>
        </Card>
      </div>
    );
  }
}

export default EducationItem;
