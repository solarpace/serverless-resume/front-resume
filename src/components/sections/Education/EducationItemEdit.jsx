import React from "react";
import Card from "../../ui/Card";
import Input from "../../ui/Input";
import Button from "../../ui/Button";

import classes from './EducationItem.module.css'
const EducationItemEdit = (props) => {
  let item = props.item;

  const submitHandler = (event) => {
  }
  const inputChangeHandler = () => {

  }
  const validateInputHandler = () => {

  }

  return (
    <div>
            <form onSubmit={submitHandler}>

      <Card className="profiles">
        <div className="jd">
          <div className="row">
            <div className="col-xs-2">
              <Input
                key="id"
                value={item.dates}
                id="education_date"
                type="text"
                onChange={inputChangeHandler}
                onBlur={validateInputHandler}
      
              />
            </div>
            <div className="col-xs-10">
              <h5>
                <Input
                key="title"
                value={item.title}
                id="education_title"
                type="text"
                onChange={inputChangeHandler}
                onBlur={validateInputHandler}
      
              />
                <Input
                key="school"
                value={item.school}
                id="education_school"
                type="text"
                onChange={inputChangeHandler}
                onBlur={validateInputHandler}
      
              />

              </h5>
            </div>
          </div>
          <div className={classes.actions}>
      <Button type="submit" className={classes.btn}>
        Save
      </Button>
    </div>
        </div>
      </Card>
      </form>
    </div>
  );
};

export default EducationItemEdit;
