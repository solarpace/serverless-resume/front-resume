import EducationItem from "./EducationItem";
import EducationItemEdit from "./EducationItemEdit";
import ScrollableAnchor from "react-scrollable-anchor";
import React, { useState, useEffect } from "react";
import Card from "../../ui/Card";
import Button from "../../ui/Button";

import { getSoda } from "../../../oci_helper";

const Education = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [isEditMode, setIsEditMode] = useState(false);

  useEffect(() => {
    const getData = async () => {
      try {
        const profile = await getSoda("educations");
        console.log(profile)
        setData(profile);
      } catch (err) {
        console.log("ERROR");
        setError(err);
        console.log(error);
      }
    };
    getData();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const editHandler = () => {
    setIsEditMode(prev => {
      return !prev;
    })  
  }

  return (
    <div>
      <Card className="profiles">
        <div className="row section">
          <ScrollableAnchor id={"sectEdu"}>
            <div className="col-xs-2">
              <h2>Education</h2>
              <Button onClick={editHandler}>{isEditMode ?  "Cancel" : "Edit"} </Button>
            </div>
          </ScrollableAnchor>
          <div className="col-xs-10">
            {data.length === 0 ? (
              <div id="spinnerHeader" className="container">
                <div className="loading"></div>
              </div>
            ) : (
              ""
            )}
            {data.map((item) => (
              <div key={item.id}>
                {
                    isEditMode ? <EducationItemEdit item={item} /> : <EducationItem item={item}  />
                }
              </div>
            ))}
            Various computing courses attended including a number of technical
            courses given at the Oracle University (Cloud IaaS, CNC Foundation,
            Systems Administration, ODI) and QAD university.
          </div>
        </div>
      </Card>
    </div>
  );
};

export default Education;
