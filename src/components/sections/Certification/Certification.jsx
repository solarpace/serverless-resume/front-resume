import ScrollableAnchor from 'react-scrollable-anchor';
import React, { useState, useEffect } from "react";

import { getSoda } from "../../../oci_helper"

const Certification = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    const getData = async () => {
      try {
        const profile = await getSoda("certifications");
        setData(profile);
      } catch (err) {
        console.log("ERROR");
        setError(err);
        console.log(error);
      }
    };
    getData();
  // eslint-disable-next-line react-hooks/exhaustive-deps
}, [error]);

    return (
      <div className="row section">
        <ScrollableAnchor id={'sectCert'}>
          <div className="col-xs-2"><h2>Certifications</h2></div>
        </ScrollableAnchor>
        <div className="col-xs-10">
          {data.length === 0 ? (
            <div id="spinnerHeader" className="container">
              <div className="loading"></div>
            </div>
          ) : (
            ""
          )}

          {data.map(item => (
            <iframe key={item.id}
              style={{  width: '300px',height: '270px'}}
              name="acclaim-badge" allowtransparency="true" frameBorder="0" id={"embedded-badge-" + item.id}
              scrolling="no" src={"https://www.credly.com/embedded_badge/" + item.badgeid}
              title="View my verified achievement on Credly." ></iframe>
          ))}

        </div>
      </div>
    )
}

export default Certification;