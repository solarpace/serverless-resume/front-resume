import React, { useState, useEffect } from "react";
import ScrollableAnchor from "react-scrollable-anchor";
import { getSoda } from "../../../oci_helper";
import Card from "../../ui/Card";

const Language = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    const getData = async () => {
      try {
        const profile = await getSoda("languages");
        setData(profile);
      } catch (err) {
        console.log("ERROR");
        setError(err);
        console.log(error);
      }
    };
    getData();
  // eslint-disable-next-line react-hooks/exhaustive-deps
}, []);

  return (
    <div>
      <Card className="profiles">
        <div className="row section">
          <ScrollableAnchor id={"sectLang"}>
            <div className="col-xs-2">
              <h2>Languages</h2>
            </div>
          </ScrollableAnchor>
          <div className="col-xs-10">
            {data.length === 0 ? (
              <div id="spinnerHeader" className="container">
                <div className="loading"></div>
              </div>
            ) : (
              ""
            )}

            {data.map((item) => (
              <div key={item.name}>
                <span>
                  <b>{item.name}</b> : {item.level}
                </span>
              </div>
            ))}
          </div>
        </div>
      </Card>
    </div>
  );
};

export default Language;
