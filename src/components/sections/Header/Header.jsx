import React, { useState, useEffect } from "react";

import { getSoda } from "../../../oci_helper";

const Header = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    const getData = async () => {
      try {
        const profile = await getSoda("profiledetails");
        setData(profile);
      } catch (err) {
        console.log("ERROR");
        setError(err);
        console.log(error);
      }
    };
    getData();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      {data.map((person) => (
        <div key={person.id} className="row header">
          {data.length === 0 ? (
            <div id="spinnerHeader" className="container">
              <div className="loading"></div>
            </div>
          ) : (
            ""
          )}

          <div>
            <div className="col-xs-2">
              <img
                className="img-circle"
                src="/img/resume_picture.png"
                alt="mypicture"
                width="180"
              />
            </div>
            <div className="col-xs-8">
              <h1 className="name">{person.name}</h1>
              <div className="role">{person.profession}</div>
              <br />
              <h4 className="profession">{person.headline}</h4>
            </div>

            <div className="col-xs-2">
              <ul className="list-unstyled">
                <li>
                  <i className="fa fa-map-marker" /> {person.location}
                </li>
                <li>
                  <i className="fa fa-flag-o" /> {person.citizenship}
                </li>
                <li>
                  <i className="fa fa-birthday-cake" /> {person.birthday}
                </li>
                <li className="link">
                  <i className="fa fa-envelope-o" />
                  <a href={"mailto:" + person.email}> {person.email}</a>
                </li>
                <li className="link">
                  <i className="fa fa-laptop" />
                  <a href={"https://www." + person.site}> {person.site}</a>
                </li>
                <li>
                  <i className="fa fa-phone" /> {person.phone}
                </li>
                <li className="link">
                  <i className="fa fa-gitlab" />
                  <a href={"https://gitlab.com/" + person.gitlab}>
                    {" "}
                    {person.gitlab}
                  </a>
                </li>
                <li className="link">
                  <i className="fa fa-linkedin" />
                  <a href={"https://www.linkedin.com/in/" + person.linkedin}>
                    {" "}
                    {person.linkedin}
                  </a>
                </li>
                <li className="link">
                  <i className="fa fa-twitter" />
                  <a href={"https://twitter.com/" + person.twitter}>
                    {" "}
                    {person.twitter}
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div className="col-xs-12 intro">
            <div>
              <a className="anchor" href="#sectSkill">
                Skills
              </a>{" "}
              -{" "}
              <a className="anchor" href="#sectCar">
                Career
              </a>{" "}
              -{" "}
              <a className="anchor" href="#sectEdu">
                Education
              </a>{" "}
              -{" "}
              <a className="anchor" href="#sectCert">
                Certifications
              </a>{" "}
              -{" "}
              <a className="anchor" href="#sectLang">
                Languages
              </a>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Header;
