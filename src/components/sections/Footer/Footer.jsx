import React from "react";

const Footer = () => {
    return (
      <div className="row footer">
        <div className="col-xl-6">
          Serverless Resume built using React, Autonomous Json DB, API Gateway, function and hosted on Oracle OCI Bucket
        </div>
        <div className="col-xl-6">
          Many thanks to <a href="https://devopstar.com/author/nathan/" rel="noopener noreferrer" target="_blank">Nathan Glover</a> who built the fondation for this : see his <a href="https://github.com/t04glovern/aws-amplify-resume" rel="noopener noreferrer" target="_blank">aws-amplify-resume</a> repository.
        </div>
      </div>
    );
}

export default Footer;
