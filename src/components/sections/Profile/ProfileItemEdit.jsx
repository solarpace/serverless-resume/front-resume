import React  from "react";
import TextArea from "../../ui/TextArea";
import classes from './ProfileItem.module.css';
import Button
 from "../../ui/Button";
const ProfileItemEdit = (props) => {

    let item = props.item;
    const submitHandler = (event) => {
    }
    const inputChangeHandler = () => {

    }
    const validateInputHandler = () => {

    }
    return (
      <>
      <form onSubmit={submitHandler}>
      {item.summary.map((itemm, i) => {
        return <TextArea
          key={i}
          value={itemm}
          id={`"profile" + ${i}`}
          type="text"
          rows="3"
          cols="100"
          onChange={inputChangeHandler}
          onBlur={validateInputHandler}
        />
      })
    }
    <div className={classes.actions}>
      <Button type="submit" className={classes.btn}>
        Save
      </Button>
    </div>
  </form>
      </>
    );
}

export default ProfileItemEdit;
