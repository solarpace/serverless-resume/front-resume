import React, { useState, useEffect } from "react";
import Card from "../../ui/Card";
import Button from "../../ui/Button";
import ProfileItem from "./ProfileItem";
import ProfileItemEdit from "./ProfileItemEdit";
import { getSoda } from "../../../oci_helper"

const Profile = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [isEditMode,setIsEditMode] = useState(false);

  useEffect(() => {
    const getData = async () => {
      try {
        const profile = await getSoda("profiles");
        setData(profile);
      } catch (err) {
        console.log("ERROR");
        setError(err);
        console.log(error);

      }
    };
    getData();
  // eslint-disable-next-line react-hooks/exhaustive-deps
}, []);


  const editHandler = () => {
    setIsEditMode(prev => {
      return !prev;
    })  
  }

  return (
    <div>
      <Card className="profiles">
      <div className="row section">
      <div className="col-xs-2">
        <h2>Profile</h2>
        <Button onClick={editHandler}>{isEditMode ?  "Cancel" : "Edit"} </Button>
      </div>
      <div className="col-xs-10">
        <div className="jd">
          <div className="row">
            {data.map((item) => (
              <div key={item.id}>
                <div className="col-xs-12">
                  {error ? (
                    <div>Oh no! Something went wrong</div>
                  ) : (
                    isEditMode ? <ProfileItemEdit item={item} /> : <ProfileItem item={item}  />
                  )}
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
      </Card>
    </div>

  );
};

export default Profile;
