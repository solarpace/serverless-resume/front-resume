import React  from "react";

const ProfileItem = (props) => {

    let item = props.item;

    return (
      <>
      {item.summary.map((itemm, i) => {
        return <p key={i}>{itemm}</p>
      })
    }
      </>
    );
}

export default ProfileItem;
