import React, { Component } from "react";
import Card from "../../ui/Card";

class CareerItem extends Component {
  render() {
    let item = this.props.item;
    
    return (
      <div>
      <Card className="profiles-item">
        <div className="row">
          <div className="col-xs-6">
            <h4>
              <b>{item.company}</b>
            </h4>
          </div>
          <div className="col-xs-6 pull-right">
            <h5 className="pull-right">
                {item.dates}
            </h5>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <b>{item.position}</b> {item.summary}
            <p></p>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            {item.accomplishments.length > 0 ? (
              <b>Selected accomplishments: </b>
            ) : null}
            <ul>
              {item.accomplishments.map((itemm, i) => (
                <li key={i} >{itemm}</li>
              ))}
            </ul>
            <div className="col-xs-3 row jd">
              {item.competencies.length > 0 ? <b>Technical competencies: </b> : null}
            </div>
            <div className="col-xs-9">
              {item.competencies.map((itemm, i) => (
                <span key={i} className="badge">{itemm}</span>
              ))}
            </div>
            <p></p>
            <div className="col-xs-3 row jd">
            {item.customers.length > 0 ? <b>Customers references: </b> : null}
            </div>
            <div className="col-xs-9">
              {item.customers.map((itemm, i) => (
                <span key={i} className="badge customer">{itemm}, </span>
              ))}
            </div>
          </div>
        </div>
        </Card>
      </div>
    );
  }
}

export default CareerItem;
