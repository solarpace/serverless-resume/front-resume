import CareerItem from "./CareerItem";
import ScrollableAnchor from "react-scrollable-anchor";
import React, { useState, useEffect } from "react";
import Card from "../../ui/Card";

import { getSoda } from "../../../oci_helper";

const Career = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    const getData = async () => {
      try {
        const profile = await getSoda("jobs");
        setData(profile);
      } catch (err) {
        console.log("ERROR");
        setError(err);
        console.log(error);
      }
    };
    getData();
  // eslint-disable-next-line react-hooks/exhaustive-deps
},[]);

  return (
    <div>
      <Card className="profiles">
        <div className="row section">
          <ScrollableAnchor id={"sectCar"}>
            <div className="col-xs-2">
              <h2>Career</h2>
            </div>
          </ScrollableAnchor>
          <div className="col-xs-10">
            <div className="jd">
              <div className="row">
                {data.length === 0 ? (
                  <div id="spinnerHeader" className="container">
                    <div className="loading"></div>
                  </div>
                ) : (
                  ""
                )}

                {data.map((item) => (
                  <div key={item.id}>
                    <CareerItem item={item} />
                    <hr />
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </Card>
    </div>
  );
};

export default Career;
