import React, { useRef, useImperativeHandle }  from "react";

import classes from "./Input.module.css";

const TextArea = React.forwardRef((props, ref) => {

    const inputRef = useRef();

    const activate = () => {
        inputRef.current.focus();
    }

    useImperativeHandle(ref, () => {
        return {
         focus: activate
    }});

  return (
        <div
          className={`${classes.control} ${
            props.isValid === false ? classes.invalid : ""
          }`}
        >
          <textarea
            type={props.type}
            ref={inputRef}
            id={props.id}
            rows={props.rows}
            cols={props.cols}
            value={props.value}
            onChange={props.onChange}
            onBlur={props.onBlur}
          />
        </div>
  );
});

export default TextArea;