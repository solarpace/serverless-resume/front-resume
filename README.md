[![pipeline status](https://gitlab.com/solarpace/serverless-resume/front-resume/badges/main/pipeline.svg)](https://gitlab.com/solarpace/serverless-resume/front-resume/-/commits/main)

# frontend-resume

Frontend React application to display a resume based on data stored in Autonomous Json database (AJD) and retrieved using Oracle Functions.

See project [fn-getresume](https://gitlab.com/solarpace/serverless-resume/front-resume) for the backend part.

👋 I am a Newbie on React development so be tolerant with my code which may not be Production ready. My objective here is to demonstrate a Use case involving a frontend website and a serveless backend getting its data from an Autonomous Json Database (AJD) through Function and API Gateway.

This application calls the API to retrieve the different parts of your resume and display them on a webpage deployed in a bucket:

![](img/myresume.png)


🍻 Many thanks to [Nathan Glover](https://devopstar.com/author/nathan/) who built the fondation for this : see his [aws-amplify-resume](https://github.com/t04glovern/aws-amplify-resume) repository.

## Prepare your OCI & Fn environment

Review the *fn-getresume* project to prepare the AJD, the Functions application and deploy the function.

Create an **API Gateway** and a Deployment pointing to your A*pplication->Function*:

![](img/apigw-screenshot.png)

Test your API with a curl command like:

```bash
curl -X POST --data '{"collection":"languages"}'  -H "Content-Type: application/json" --location 'https://btvrjcexxxxxxxxxxxx.apigateway.eu-frankfurt-1.oci.customer-oci.com/resume/getdata'
```

You should have a result like :

```json
[
  {
    "id": "1",
    "level": "mother tongue",
    "name": "French"
  },
  {
    "id": "2",
    "level": "fluent",
    "name": "English"
  }
]
```

*Note* : you should also configure some authentication but I won't cover it here for simplicity. You can find all the information here : [Adding Authentication and Authorization to API Deployments](https://docs.oracle.com/en-us/iaas/Content/APIGateway/Tasks/apigatewayaddingauthzauthn.htm)

Make sure your environment is correctly configured with **OCI Cli**; you can check by running the following command to retrieve your OCI Object Storage namespace:

```bash
oci os ns get
```

If you need to install the **OCI Cli**, you can find all the required steps here : [OCI Cli Quickstart](https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/cliinstall.htm)

You'll also need to create a bucket in Object Storage where you'll deploy your static web frontend.

**Note**: I created here a public bucket but for a production application you should create a private bucket, configure the API Gateway as a frontend for your static files and deploy a WAF to protect your website. [*Christopher Johnson*](https://www.ateam-oracle.com/chris-johnson), from the *A-Team* akready wrote a post describing all the steps: [Creating a web site with Object Storage, Functions, and the API Gateway](https://www.ateam-oracle.com/creating-a-web-site-with-object-storage%2C-functions%2C-and-the-api-gateway)


## Getting Started

The first step is to git clone this project: https://gitlab.com/solarpace/serverless-resume/frontend-resume.git (`git clone git@gitlab.com:solarpace/serverless-resume/frontend-resume.git`)

Next, update the photo with your own: **public/img/resume_picture.png**

Finally, create the **src/oci-exports.yaml** file with your OCI information:

* oci_project_region
* oci_apigateway_url
* oci_apigateway_apiKey
* oci_os_namespace
* oci_os_bucket

```js
const ociconfig = {
    "oci_project_region": "eu-frankfurt-1",
    "oci_apigateway_url": "https://xxxxxxx.apigateway.eu-frankfurt-1.oci.customer-oci.com/resume/getdata",
    "oci_apigateway_apiKey": "da2-xxxxxxxx",
    "oci_os_namespace": "frdxxxxxxx",
    "oci_os_bucket": "serverless-resume"
};
export default ociconfig;

```

## OCI deployment

The deployment could be done using the script **deploy_oci.sh** which runs the following steps:

* npm run build <== build the static website locally
* replace embedded URLs with Object Storage compliant URLs, for example : */img/image.png* to */namespace/b/bucket-name/o/img/image.png*
* upload all files in the Object Storage bucket


### CI/CD

I also built some deployment automation thanks to Gitlab CI/CD features.

The *.gitlab-ci.yml* file will automatically build and redeploy the website when you push your changes.

#### CI/CD variables

You first need to define those variables in GitLab CI/CD settings:

![CI/CD Variables](img/gitlab-cicd-variables.png)

| Variable Name            | Description | Example  |
|--|--|--|
| OCI_EXPORTS_FILE | the React configuration file for the backend settings     | const ociconfig = {<br/>"oci_project_region": "eu-frankfurt-1"<br/>"oci_apigateway_url": "https://btvrxxxxxx.apigateway.eu-frankfurt-1.oci.customer-oci.com/resume/getdata",<br/>"oci_apigateway_apiKey": "da2-6o6hawxxx",<br/>"oci_os_namespace": "frdxxxxxxx",<br/>"oci_os_bucket": "serverless-resume"<br/>};<br/>export default ociconfig;   |
| OCI_CONFIG_FILE | Fn Context file for OCI tenant    | api-url: https://functions.eu-frankfurt-1.oci.oraclecloud.com<br/>oracle.compartment-id: ocid1.compartment.oc1..xxxx<br/>oracle.profile: DEFAULT<br/>provider: oracle<br/>registry: fra.ocir.io/frdxxxxxxx/resume   |
| OCI_API_KEY | Your OCI User API Private key   | -----BEGIN RSA PRIVATE KEY-----<br/>MIIExxxxx<br/>-----END RSA PRIVATE KEY-----  |
| OCI_NAMESPACE | OCI Object Storage Namespace | frdxxxxxxx |
| OCI_OS_BUCKET | OCI Object Storage Bucket | serverless-resume |

👋 I know I defined some settings like the bucket twice - this could be improved.

Once done, each time you push your changes, the CI/CD pipeline will redeploy your function:

![CI/CD Pipeline](img/gitlab-cicd-pipeline.png)

Your bucket should be updated with the latest files:

![](img/oci-objectstorage.png)

Select the *index.html* file, option *View  Object Details* and retrieve the URL for your website:

![](img/oci-os-index-url.png)

Open this URL using another browser, in private mode to check if it is really a public URL; you should have your resume page available with all the Json data from AJD ! 🍻
